/*************************************************************************//**
 * @file
 *****************************************************************************/
#ifndef __PRIMELIST_H_
#define __PRIMELIST_H_

#include <math.h>
#include <string>
#include <iostream>
#include <time.h>
#include <stdlib.h>
#include <sstream>
#include <algorithm>

using namespace std;

/*!
*  @brief typedef is used for long ints
*/
typedef unsigned long int ull;

/*!
* @brief The Primes class creates the linked list that holds the 
* prime.
*
* @details The class can output parts of the list based upon the user's
* selecion from the print_queries.
*
*/
class Primes
{

public:
    /*!
    *  @brief The constructor, initialize the class upon creation
    */
    Primes();

    /*!
    *  @brief The destructor, destroys the class
    */
    ~Primes();

    /*!
    *  @brief destroy() deletes the linked list
    */
    void destroy();

    /*!
    *  @brief insert inserts a single prime into a linked list
    */
    void insert(ull prime);

    /*!
    *  @brief outputlist outputs the entire linked list
    */
    void outputlist(ull prime);

    /*!
    *  @brief remove_nonTwins removes the non-twin primes
    */
    void remove_nonTwins(ull prime);

    /*!
    *  @brief greater_then outputs the twinprimes greater then what the user entered
    */
    void greater_then(ull number);

    /*!
    *  @brief greater_andEqual outputs the twinprimes greater than and equal to what the user entered
    */
    void greater_andEqual(ull number);

    /*!
    *  @brief less_then outputs the twinprimes less than what the user entered
    */
    void less_then(ull number);

    /*!
    *  @brief less_andEqual outputs the twinprimes less than and equal to what the user entered
    */
    void less_andEqual(ull number);

    /*!
    *  @brief between outputs the twim prime pairs inclusively betweeen what the user entered for number and number2
    */
    void between(ull number, ull number2);

    /*!
    *  @brief random outputs random pairs of twin primes
    */
    void random(ull prime);

private:

    /*!
    *  @brief the structure for the twin primes
    */
    struct node
    {
        unsigned int prime;
        node *next;
    };
    node *headptr;

};


#endif