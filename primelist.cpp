/*************************************************************************//**
 * @file
 *****************************************************************************/
#include "primelist.h"

Primes::Primes()
{
    headptr = nullptr;
}
Primes::~Primes()
{
    
}
/**************************************************************************//**
 * @author Brian Jenkins
 *
 * @par Description:
 * This function inserts a prime into the linked list and checks to make sure
 * that the primes are inserted in order least to most.
 *
 * @param[in]   prime - the prime number that was generated from the upperbound
 *                      to lower bound
 *
 *****************************************************************************/
void Primes::insert(ull prime)
{
    node *curr = headptr;
    node *temp = new node;

    //sets temp node to prime
    temp->prime = prime;
    //sets next node to null
    temp->next = nullptr;
   
    //makes sure list stays in order
    if (headptr == nullptr || headptr->prime >= temp->prime)
    {
        temp->next = headptr;
        headptr = temp;
        return;
    }
    //makes sure that the current node and the next node are not headptr
    while (curr != nullptr && curr->next != nullptr)
    {
        if (curr->next->prime >= temp->prime)
        {
            break;
        }
        else
        {
            //sets current to next node
            curr = curr->next;
        }
    }
     //makes sure nodes go in the right place
     temp->next = curr->next;
     curr->next = temp;
}
/**************************************************************************//**
 * @author Brian Jenkins, Amber Steele
 *
 * @par Description:
 * This function outputs the linked list of all primes. 
 *
 * @param[in]   prime - the prime number that was generated from the upperbound
 *                      to lower bound
 *
 *****************************************************************************/
void Primes::outputlist(ull prime)
{
    node* temp;

    temp = headptr;

    cout << endl;
    cout << "List: " << endl;

    //walks through linked list and outputs one by one
    while(temp != nullptr)
    {
        cout << temp->prime <<" ";
        temp = temp->next;//sets temp to next node
    }
    cout << endl;
}
/**************************************************************************//**
 * @author Brian Jenkins
 *
 * @par Description:
 * This removes the non twin primes. It compares current current of next and 
 * current of next of next. It then subtracts them to figure out if they are
 * twin primes or not.
 *
 * @param[in]   prime - the prime number that was generated from the upperbound
 *                      to lower bound
 *
 *****************************************************************************/
void Primes::remove_nonTwins(ull prime)
{
    node* curr;
    node* previous;

    curr = headptr;

    while (curr != nullptr)
    {
        if (curr->next == nullptr)
            return;

        //checks to see if upcoming pointer is nullptr
        if (curr->next->next == nullptr)
        {
            //makes sure last number is a co-prime
            if ((curr->next->prime - curr->prime) > 2)
            {
                //sets the current node that will be the last node to previous
                previous = curr;
                //takes current node and makes it next
                curr = curr->next;
                //takes the previous of next node and set that equal to the current of next node
                previous->next = curr->next;
                //deletes curr
                delete curr;
                //sets current node to previous
                curr = previous;
                //all co-primes are deleted
                return;
            }
        }
        //looking at 2 number(should be middle) middle is temp->next->prime
        //    2nd           minus       1st------------3rd             minus           2nd
        while((curr->next->prime - curr->prime > 2) && (curr->next->next->prime - curr->next->prime > 2))
        {
            //sets the current node that will be the last node to previous
            previous = curr;
            //takes current node and makes it next
            curr = curr->next;
            //takes the previous of next node and set that equal to the current of next node
            previous->next = curr->next;
            //deletes curr
            delete curr;
            //sets current node to previous
            curr = previous;
            //checks to see if upcoming pointer is nullptr
            if (curr->next->next == nullptr)
            {
                //makes sure last number is a co-prime
                if ((curr->next->prime - curr->prime) > 2)
                {
                    //sets the current node that will be the last node to previous
                    previous = curr;
                    //takes current node and makes it next
                    curr = curr->next;
                    //takes the previous of next node and set that equal to the current of next node
                    previous->next = curr->next;
                    //deletes curr
                    delete curr;
                    //sets current node to previous
                    curr = previous;
                    //all co-primes are deleted
                    return;
                }
            }
        }
        //moves node to next
        curr = curr->next;
    }
}
/**************************************************************************//**
 * @author Brian Jenkins
 *
 * @par Description:
 * This function displays a random pair of twin primes. It is seeded through 
 * the systems time and then seeded again through the systems time. The headptr 
 * is moved up based on the random number.
 *
 * @param[in]   prime - the prime number that was generated form the upperbound
 *                      to lower bound
 *
 *****************************************************************************/
void Primes::random(ull prime)
{
    int random_seed = 0;
    srand(time(NULL));
    node* temp;
    node* previous;
    int i = 0;
    bool yes = false;

    //seeds in random numbers and doesnt create seed that are too high
    random_seed = rand();
    while (random_seed > ((prime) / 20))
        random_seed = rand();

    temp = headptr;//sets temp node to head pointer

    cout << endl;
    cout << "Random: " << endl;

    //this loop traverses the linked list until the random number has been reached
    while (i != random_seed)
    {
        //makes sure bounds are not over stepped
        if ((temp->next == nullptr) || (temp->next->next == nullptr))
        {
            break;
            yes = true;
        }
        previous = temp;
        temp = temp->next;
        i++;
        yes = true;
    }
    if (yes == true)
    {
        //this statement puts out the random pairs based on if they are twin primes
        if (temp->next->prime - temp->prime > 2)
            cout << "( " << previous->prime << " , " << temp->prime << " ) " << endl;
        else
            cout << " ( " << temp->prime << " , " << temp->next->prime << " ) " << endl;
    }
    if (temp->next == nullptr)
    {
        cout << "Only one prime in list. " << "Prime: " << temp->prime << endl;
    }
    cout << endl;
}
/**************************************************************************//**
 * @author Brian Jenkins
 *
 * @par Description:
 * This function outputs the twin prime pairs greater than what the user entered.
 * If the pair does not exits then nothing is displayed.
 *
 * @param[in]      number - The number that the user entered
 *
 *****************************************************************************/
void Primes::greater_then(ull number)
{
    node* temp;
    node* previous;

    temp = headptr;

    while ((temp->prime < number + 1) && temp->next != nullptr)
    {
        temp = temp->next;
        previous = temp;   
    }
    while (temp != nullptr || temp->next != nullptr)
    {   
        if (temp == nullptr || temp->next == nullptr)
        {
            break;
        }
        if (temp->next->prime - temp->prime > 2)
        {
            previous = temp;
            temp = temp->next;
        }
        else
        {
            cout << " ( " << temp->prime << " , " << temp->next->prime << " ) " << endl;
            temp = temp->next;
        }
    }
}
/**************************************************************************//**
 * @author Brian Jenkins
 *
 * @par Description:
 * This function displays twin prime pairs greater than and equal to the number
 * that the user entered. If no twin prime pair is greater than or equal to the
 * number entered then nothing is displayed.
 *
 * @param[in]   number - The number that the user entered
 *
 *****************************************************************************/
void Primes::greater_andEqual(ull number)
{
    node* temp;
    node* previous;

    temp = headptr;

    while ((temp->prime <= number) && temp->next != nullptr)
    {
        if (temp->prime == number)
            break;
        temp = temp->next;
        previous = temp;

    }
    while (temp != nullptr || temp->next != nullptr)
    {
        if (temp == nullptr || temp->next == nullptr)
        {
            break;
        }
        if (temp->next->prime - temp->prime > 2)
        {
            previous = temp;
            temp = temp->next;
        }
        else
        {
            cout << " ( " << temp->prime << " , " << temp->next->prime << " ) " << endl;
            temp = temp->next;
        }
    }
}
/**************************************************************************//**
 * @author Brian Jenkins
 *
 * @par Description:
 * This function displays twin prime pairs less than the number
 * that the user entered. If no twin prime pair is less than the number
 * entered then nothing is displayed.
 *
 * @param[in]   number - The number that the user entered
 *
 *****************************************************************************/
void Primes::less_then(ull number)
{
    node* temp;
    node* previous;

    temp = headptr;
    
    while (temp->prime < number && temp->next != nullptr )
    {
        if (temp == nullptr && temp->next == nullptr)
        {
            break;
        }
        if (temp->next->prime - temp->prime > 2)
        {
            previous = temp;
            temp = temp->next;
        }
        else
        {
            if (temp->prime < number && temp->next->prime < number)
                cout << " ( " << temp->prime << " , " << temp->next->prime << " ) " << endl;
            temp = temp->next;
        }
    }
}
/**************************************************************************//**
 * @author Brian Jenkins
 *
 * @par Description:
 * This function displays twin prime pairs less than and equal to the number
 * that the user entered. If no twin prime pair is less than to or equal the number
 * entered then nothing is displayed.
 *
 * @param[in]   number - The number that the user entered
 *
 *****************************************************************************/
void Primes::less_andEqual(ull number)
{
    node* temp;
    node* previous;

    temp = headptr;

    while (temp->prime < number && temp->next != nullptr)
    {

        if (temp == nullptr && temp->next == nullptr)
        {
            break;
        }
        if (temp->next->prime - temp->prime > 2)
        {
            previous = temp;
            temp = temp->next;
        }
        else
        {
            if (temp->prime <= number && temp->next->prime <= number)
                cout << " ( " << temp->prime << " , " << temp->next->prime << " ) " << endl;
            temp = temp->next;
        }
    }
}
/**************************************************************************//**
 * @author Brian Jenkins
 *
 * @par Description:
 * This function displays the twin primes that are between the two numbers
 * that the user enters in. If no twin prime pairs are between the entered
 * numbers then nothing is displayed.
 *
 * @param[in]   number - number that the user entered  
 * @param[in]   number2 - second number that the user entered  
 *
 *****************************************************************************/
void Primes::between(ull number, ull number2)
{
    node* temp;
    node* previous;

    temp = headptr;

    while (temp->prime < number)
        temp = temp->next;
    while ((temp->prime < number2 - 1) && temp->next != nullptr)
    {
        if (temp->next->prime - temp->prime > 2)
        {
            previous = temp;
            temp = temp->next;
        }
        else
        {
            cout << " ( " << temp->prime << " , " << temp->next->prime << " ) " << endl;
            temp = temp->next;
            previous = temp;
        }
    }
}
/**************************************************************************//**
 * @author Brian Jenkins
 *
 * @par Description:
 * This function goes through the linked list one by one and deletes them.
 *
 *****************************************************************************/
void Primes::destroy()
{
    struct node* next;
    struct node* curr = headptr;

    //walks through linked list and destorys it one by one
    while (curr != nullptr)
    {
        next = curr->next;
        delete curr;
        curr = next;
    }
    headptr = nullptr;
}