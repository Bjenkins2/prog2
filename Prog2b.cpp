/*************************************************************************//**
 * @file
 *
 * @mainpage Prog2b.cpp
 *
 * @section course_section Course Information
 *
 * @author Brian Jenkins, Amber Steele, Ryan Bordeaux
 *
 * @date 10/30/2014
 *
 * @par Professor:
 *         Roger Schrader
 *
 * @par Course:
 *         CSC 250
 *
 * @par Location:
 *         Classroom Bulding - Room 107
 *
 * @section program_section Program Information
 *
 * @details This program is designed to generate twin prime numbers. The
 * interface will allow a user to enter numerous queries that he/she may want
 * to view. This program will be able to handle primes up to 4 billion. 
 *
 * At the command prompt a user will only enter the upper bound (at most 4 billion).
 * Error checking will occur and the program will output appropriate messages
 * if needed. The program will then generate a list of prime number between 3
 * and the upper bound. After the list is generated, the program will remove
 * all the non-twin primes from the list of primes. A twin prime consists of a
 * prime and prime + 1 (i.e. prime, prime+1). Once the list contains only the
 * non-twin primes, the user will then be able to enter a query command, which
 * the program will be able to support. Commands that will be supplied are:
 * < num, <= num, > num, >= num, <> num1 num2, “all”, “random”, “help”, and “exit”.
 * 
 * The program will continue to accept queries until the user enters “exit” or “quit”.
 *
 * @section compile_section Compiling and Usage
 *
 * @par Compiling Instructions:
 *
 * @par Usage:
   @verbatim
   c:\> prog1.exe upperbound #
   d:\> c:\bin\prog1.exe
   @endverbatim
 *
 * @section todo_bugs_modification_section Todo, Bugs, and Modifications
 *
 * @bug none
 *
 * @todo none
 *
 * @par Modifications and Development Timeline:
   @verbatim
   Date           Modification
   -------------  -------------------------------------------------------------
   Oct 31, 2014  Got command line arguments working, insertion working
   Nov  2, 2014  delete non twin primes, output all working, <, <=
                 >, >= and <> working, bug fixes and documentation done
   @endverbatim
 *
 *****************************************************************************/
#include <math.h>
#include <string>
#include <iostream>
#include <time.h>
#include <stdlib.h>
#include <sstream>
#include <algorithm>
#include <list>

using namespace std;

typedef unsigned long int ull;

list<ull> Primes;

int is_prime(ull prime_num, ull num);
int enter_num(ull num, ull prime_num);
int queries(ull num, ull prime_num);
void print_queries();
void all(ull num);
void delete_nonTwins(ull num);
void greater(ull num, ull number);
void greater_thenEqual(ull num, ull number);
void less_then(ull num, ull number);
void less_thenEqual(ull num, ull number);
void between(ull num, ull number, ull number2);
void random(ull num);

/**************************************************************************//**
 * @author Brian Jenkins
 *
 * @par Description:
 * This function gets the command line arguments and passes the upperbound into
 * a variable that is used to find the primes from 3 to that upperbound. To do 
 * so, this function also calls other functions.
 *
 * @param[in]   argc - a count of the command line arguments used to start
 *                        the program.
 * @param[in]   argv - a 2d character array of each argument. Each token
 *                        occupies one line in the array.
 *
 * @returns 0 program ran successful.
 * @returns 1 command line arguments were not correctly given
 *            number was not in lower and upperbound range
 *
 *****************************************************************************/
int main(int argc, char**argv)
{
    ull prime_num = 3;
    char *end;
    unsigned long int num = 0;
    int i = 0;
    int j = 0;
    
    //makes sure sufficient command line arguments are given
    if (argc <= 1)
    {
        cout << "No command line arguments given." << endl;
        cout << "Usage: Prog2a.exe <upperbound #>" << endl;
        return 1;
    }

    //loops through to get number in argv argument
    for (i = 0; i < argc; i++)
    {
        //changes char to int
        num = strtoul(argv[i], &end, 0);
    }

    //checks to make sure number is in range.
    if (num < 3 || num > 4000000000)
    {
        cout << "The number must be between 3 and 4 billon!" << endl;
        return 1;
    }
    
    //finds primes and inserts them into linked list
    enter_num(num, prime_num);
        
    //show the queries
    queries(num, prime_num);
    
    //destroys nodes
    Primes.clear();
    
    return 0;
}
/**************************************************************************//**
 * @author Brian Jenkins
 *
 * @par Description:
 * This function takes prime numbers starting from the upperbound and inserts
 * them into the list. If the number is not a prime then it is not inserted 
 * into the list.
 *
 * @param[in]   num - upperbound number   
 * @param[in]   prime_num - this number starts at 3
 *
 * @returns 0 program ran successful.
 *
 *****************************************************************************/
int enter_num(ull num, ull prime_num)
{
    ull i = 0;

    if (num % 2 == 0) num--;
    //outputs list backwards
    for (i = num; i >= prime_num; i -= 2)
    {
        //if it is a prime then it is inserted
        if (is_prime(i, num) == 1)
        {
            Primes.push_front(i);
        }
    }
    return 0;
}
/**************************************************************************//**
 * @author Brian Jenkins
 *
 * @par Description:
 * This function finds the primes from the upper bound to 3. The function starts
 * at the upperbound then everytime a prime is found the prime is inserted into 
 * the list. This function checks to see if the number is divisable by any number
 * less then the sqrt of n. If it is then 1 is returned.
 *
 * @param[in]   prime_num - this number starts at 3
 * @param[in]   num - upperbound number
 *
 * @returns 0 function ran successful.
 * @returns 1 number is not a prime.
 *
 *****************************************************************************/
int is_prime(ull prime_num, ull num)
{
    ull i = 0;
    ull n = 0;
    
    //takes number in question
    for (i = prime_num; i <= num;)
    {
        //finds a number divisible by number in question
        for (n = 2; n <= (ull)sqrt((double)i); n++)
        {
            //see if number is divisble 
            if ((i % n) == 0)
            {
                //if it is returns 0
                return 0;
            }
        }
        //if number is prime a 1 is returned
        return 1;
    }
    return 0;
}
/**************************************************************************//**
 * @author Amber Steele, Brian Jenkins
 *
 * @par Description:
 * This function is for the queries. It allows the user to enter <, <=
 * >, >=, <>, a space, and then a number. This is put into a string.
 * Then based on what the user entered the query displays what the user wants.
 *
 * @param[in]  num - upperbound number    
 * @param[in]  prime_num -  number that starts at 3
 *
 * @returns 0 program ran successful.
 * @returns 1 command line arguments were not correctly given
 *
 *****************************************************************************/
int queries(ull num, ull prime_num)
{
    int pos = 0;
    string q = "";
    string first = "";
    string user_nums2 = "";
    string user_nums = "";
    short end = 0;
    ull number = 0;
    ull number2 = 0;
    
    while (end != -1)
    {
        cout << "Enter a command for the twin primes. Enter 'help' for help." << endl;
        getline(cin, q);
        first = q.substr(0, 2);
        first.erase(remove_if(first.begin(), first.end(), isspace), first.end());
        
        if (q != "random" && q != "help" && q != "all")
        {
            if (first == ">" || first == ">=" || first == "<" || first == "<=" || first == "<>")
            {
                q.erase(0, 1);
                user_nums = q.substr(1);
                if (first == "<>")
                {
                    pos = q.find(" ", q.find(" ") + 1);
                    user_nums2 = q.substr(pos);
                    istringstream(user_nums) >> number;
                    istringstream(user_nums2) >> number2;
                }
                istringstream(user_nums) >> number;
            }
        }
        //will output the twin primes greater than num
        if (first == ">") greater(num,number);
           
        //will output the twin primes greater than num
        else if (first == ">=") greater_thenEqual(num, number);
         
        //will output the twin primes less than num
        else if (first == "<") less_then(num, number);
        
        //will output the twin primes less than num
        else if (first == "<=") less_thenEqual(num, number);
       
        //will output all the twin prime pairs between num1 and num2 inclusive
        else if (first == "<>")  between(num, number, number2);
       
        //outputs list
        else if (q == "all") all(num);
       
        //will show the queries that the user can do with an explanation
        else if (q == "help") print_queries();
        
        //will output a twin prime pair within the list
        else if (q == "random")
            random(num);
        
        else if (q == "exit" || q == "quit")
        {
            cout << "Program is exiting/quitting the queries!" << endl;
            return end = -1;
        }
        else
        {
            cout << "Incorrect command!" << endl;
            q.clear();
        }
        cout << endl;
    }
    q.clear();
    return 0;
}

/**************************************************************************//**
 * @author Amber Steele
 *
 * @par Description:
 * This function displays the help menu.
 *
 *****************************************************************************/
void print_queries()
{
    cout << endl;
    cout << "Enter >, >=, <, <= THEN A SPACE, then number" << endl;
    cout << endl;
    cout << "> num         outputs the twin primes greater than num" << endl;
    cout << ">= num        outputs the twin primes greater than num" << endl;
    cout << "< num         outputs the twin primes less than num" << endl;
    cout << "<= num        outputs the twin primes less than num" << endl;
    cout << "<> num1 num2  outputs ALL the twin prime pairs between num1 and num2 inclusively" << endl;
    cout << "all           outputs ALL the twin primes in the list" << endl;
    cout << "help          shows the queries that the user can do with an explanation" << endl;
    cout << "random        outputs a twin prime pair within the list" << endl;
    cout << "exit          will exit the queries" << endl;
    cout << endl;
}
/**************************************************************************//**
 * @author Brian Jenkins
 *
 * @par Description:
 * This function displays all the twin primes within the list.
 *
 *****************************************************************************/
void all(ull num)
{
    //removes non twin primes
    delete_nonTwins(num);
    
}
/**************************************************************************//**
 * @author Amber Steele
 *
 * @par Description:
 * This function deletes all the non twin primes within the list.
 *
 *****************************************************************************/
void delete_nonTwins(ull num)
{
    cout << "Twin primes: " << endl;
    for (ull current = 3; current < num - 2; current += 2)
    {
        if (is_prime(current, num))
        {
            if (is_prime(current + 2, num))
            {
                cout << "(" << current << ", " << (current + 2) << ")" << endl;
            }
        }
    }
}
/**************************************************************************//**
 * @author Brian Jenkins
 *
 * @par Description:
 * This function is used for the queries section. It outputs all the twin
 * primes greater than num.
 *
 *****************************************************************************/
void greater(ull num, ull number)
{
    for (ull current = 3; current < num - 2; current += 2)
    {
        if (is_prime(current, num))
        {
            if (is_prime(current + 2, num))
            {
                if (current > number)
                    cout << "(" << current << ", " << (current + 2) << ")" << endl;
            }
        }
    }
}
/**************************************************************************//**
 * @author Brian Jenkins
 *
 * @par Description:
 * This function is used for the queries section. It outputs all the twin
 * primes greater than and equal to num.
 *
 *****************************************************************************/
void greater_thenEqual(ull num, ull number)
{
    for (ull current = 3; current < num - 2; current += 2)
    {
        if (is_prime(current, num))
        {
            if (is_prime(current + 2, num))
            {
                if (current >= number)
                    cout << "(" << current << ", " << (current + 2) << ")" << endl;
            }
        }
    }
}
/**************************************************************************//**
 * @author Brian Jenkins
 *
 * @par Description:
 * This function is used for the queries section. It outputs all the twin
 * primes less than num.
 *
 *****************************************************************************/
void less_then(ull num, ull number)
{
    for (ull current = 3; current < num - 2; current += 2)
    {
        if (is_prime(current, num))
        {
            if (is_prime(current + 2, num))
            {
                if (current < number - 2)
                    cout << "(" << current << ", " << (current + 2) << ")" << endl;
            }
        }
    }
}
/**************************************************************************//**
 * @author Brian Jenkins
 *
 * @par Description:
 * This function is used for the queries section. It outputs all the twin
 * primes less than and equal to num.
 *
 *****************************************************************************/
void less_thenEqual(ull num, ull number)
{
    for (ull current = 3; current < num - 2; current += 2)
    {
        if (is_prime(current, num))
        {
            if (is_prime(current + 2, num))
            {
                if (current < number)
                    cout << "(" << current << ", " << (current + 2) << ")" << endl;
            }
        }
    }
}
/**************************************************************************//**
 * @author Brian Jenkins
 *
 * @par Description:
 * This function is used for the queries section. It outputs all the twin
 * prime pairs between number and number2 inclusive.
 *
 *****************************************************************************/
void between(ull num, ull number, ull number2)
{
    for (ull current = 3; current < num - 2; current += 2)
    {
        if (is_prime(current, num))
        {
            if (is_prime(current + 2, num))
            {
                if (current >= number && current <= number2)
                    cout << "(" << current << ", " << (current + 2) << ")" << endl;
            }
        }
    }
}
/**************************************************************************//**
 * @author Brian Jenkins
 *
 * @par Description:
 * This function is used for the queries section. It outputs a random twin
 * prime pair.
 *
 *****************************************************************************/
void random(ull num)
{
    srand(time(NULL));
    ull seed_random = rand();

    while (seed_random > num)
        seed_random = rand() % 10 + 3;
    for (ull current = seed_random; current < num - 2; current += 2)
    {
        current = seed_random;
        seed_random = rand() % 10 + 3;
        if (is_prime(current, num))
        {
            if (is_prime(current + 2, num))
            {
                cout << "(" << current << ", " << (current + 2) << ")" << endl;
                return;
            }
        }
    }
}